yarPPG
================

-----------------------------------
basado en: <https://github.com/SamProell/yarppg>

**yarppg** es una implementación de rPPG en python. Funciona detectando pequeños cambios en el color de piel originado por 
la pulsación de la sangre[1].  

[1] W Verkruysse, L O Svaasand and J S Nelson. Remote plethysmographic
imaging using ambient light. *Optics Express*. 2008;16(26):21434–21445.
doi:10.1364/oe.16.021434 <https://doi.org/10.1364/oe.16.021434> 
   


Instalación y uso
----------------------

Para correr este codigo es necesario clonar este repositorio e instalar los requisitos solicitados:

```
   git clone git@gitlab.com:p3767/yarppg.git
   cd yarppg
   pip install -r requirements.txt
```

Ahora simplemente se puede ejecutar el programa como un paquete python utilizando la flag `-m`:

```
   python -m yarppg  # from yarppg top-level directory
```

El archivo outputs.csv generado una vez ejecutado el programa, entrega tiempo de medicion, ppg obtenido y colores medios de rojo, verde y azul para cada frame que se mide.  

El archivo output.mp4 entrega un video de lo que ve la camara al ejecutar el programa.  

Tanto el video como el csv son usados como parte del dataset utilizado para la predicción de presión sanguinea.


