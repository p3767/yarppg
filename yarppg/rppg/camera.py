import time

import cv2
import numpy as np
from PyQt5.QtCore import QThread, pyqtSignal


class Camera(QThread):
    """Wraps an OpenCV VideoCapture and provides a Qt signal with new
    frames.

    The :py:`run` function launches a loop that waits for new frames in
    the VideoCapture and emits them with a `new_frame` signal.  Calling
    :py:`stop` stops the loop and releases the camera.
    """

    new_frame = pyqtSignal(np.ndarray)

    def __init__(self, video=0, parent=None):
        """Initialize Camera instance

        Args:
            video (int or string): ID of camera or video filename
            parent (QObject): parent object in Qt context
        """

        QThread.__init__(self, parent=parent)
        self._cap = cv2.VideoCapture(video)
        self._running = False
        ## some videowriter props
        self._sz = (int(self._cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                int(self._cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))

        self._fps = 30
        self._fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')


        # open and set props
        self._start_point = (int(self._sz[0]*0.3), int(self._sz[1]*0.3))
        self._end_point = (int(self._sz[0]*0.7), int(self._sz[1]*0.76))
        self._vid_sz = (self._end_point[0] - self._start_point[0], self._end_point[1] - self._start_point[1])
        self._vout = cv2.VideoWriter()
        self._vout.open('output.mp4',self._fourcc,self._fps,self._vid_sz,True)
        self._cnt = 0


    def run(self):
        self._running = True
        while self._running:
            self._cnt +=1
            ret, frame = self._cap.read()

            if not ret:
                self._running = False
                raise RuntimeError("No frame received")
            else:
                ## grebación de frame a salida mp4
                frame = cv2.flip(frame, 1)
                crop_img = frame[self._start_point[1]:self._end_point[1], self._start_point[0]:self._end_point[0]]
                self._vout.write(crop_img)
                
                cv2.putText(frame, str(self._cnt), (10, 20), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,0), 1, cv2.LINE_AA)
                cv2.rectangle(frame, self._start_point, self._end_point, (255, 0, 0), 2)
                self.new_frame.emit(frame)
                
                
            



    def stop(self):
        self._running = False
        time.sleep(0.1)
        self._vout.release()
        self._cap.release()
